Individual Project: 
All files work, except for the php file. This is because Node.js does not support php-- php is an Apache language, which cannot access the directory I'm working in.
The add-ons created are:
1. Other users may press the "ban" button, even if they are not a chat owner, which will inform the owner of the chat (no matter where they are) that Person A wishes for Person B to be banned. Only the owner may ban someone or kick someone out of a chat.
2. All private messages are red.
3. A user will notify others in the room if they log off or log into a room.
4. A user may choose to permanently block another user's messages in all chat rooms if they are being abusive. You cannot block yourself.


This is where my server sits: http://ec2-52-33-157-240.us-west-2.compute.amazonaws.com:3456/
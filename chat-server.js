// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);
 

var roomarray=['main'];
var mainpeeps=[];
var mypeeps=[mainpeeps];
var roompass=[''];
var socks=[];
var ownera=['admin'];
var banlist=[mainpeeps];
//in this array, odd numbers are socketids and even numbers are their usernames
//mypeeps is an array of arrays, holds 
// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.

	socket.on('new_client', function(data){
		socks.push(socket.id);
		socks.push(data["usern"]);
		socket.join(socket.id);
	});

	socket.on('new_room', function(data){
		//takes in new room title, adds this room to array and tells all clients a new room exists
		roomarray.push(data["message"]);
		//also adds a new array to mypeeps
		mypeeps.push([]);
		roompass.push(data["psw"]);
		io.sockets.emit("add_this_room", {message:data["message"]});
		ownera.push(data["usern"]);
		banlist.push([]);
	});

	socket.on('change_me', function(data){
		//check to see if user is banned
		var isbann=false;
		for( i=0; i<banlist[roomarray.indexOf(data["message"])].length; i++){
			if (data["usern"]==banlist[roomarray.indexOf(data["message"])][i]){
				isbann=true;
				var greeta="You are banned from that room.";
				var bla='purple';
				socket.emit("message_to_client", {message: greeta, usern: data["usern"], color:bla});
			}
		}
		if(data["psw"]==roompass[roomarray.indexOf(data["message"])] && isbann==false){
		socket.leave(data["curroom"]);
		socket.join(data["message"]);
		//if the person is in the current room, cut them out of it
		if(mypeeps[roomarray.indexOf(data["curroom"])].indexOf(data["usern"]) >=0){
			mypeeps[roomarray.indexOf(data["curroom"])].splice(mypeeps[roomarray.indexOf(data["curroom"])].indexOf(data["usern"]),1);
		}
		//add person to new room's array
		mypeeps[roomarray.indexOf(data["message"])].push(data["usern"]);
		//if the current room and new room aren't the same, then run this
		//if(data["curroom"]!=data["message"]){	

			//revamp old room so it no longer has person
			io.sockets.in(data["curroom"]).emit("revamp_peep", {usern: data["usern"]});
			//go through people list in that room and add tem to all screens
			for(i=0; i<mypeeps[roomarray.indexOf(data["curroom"])].length; i++){
				var specper=mypeeps[roomarray.indexOf(data["curroom"])][i];
				io.sockets.in(data["curroom"]).emit("add_this_person", {usern: specper});
			}
			//revamp new room and add all people to all screens
			io.sockets.in(data["message"]).emit("revamp_peep", {usern: data["usern"]});

			for(i=0; i<mypeeps[roomarray.indexOf(data["message"])].length; i++){
				var specper=mypeeps[roomarray.indexOf(data["message"])][i];
				io.sockets.in(data["message"]).emit("add_this_person", {usern: specper});
			}	
		//}	
		var greeta="I've joined the chat room";
		var bya="I've left the chat room";	
		io.sockets.in(data["curroom"]).emit("message_to_client", {message: bya, usern: data["usern"], color: bya});
		io.sockets.in(data["message"]).emit("message_to_client", {message: greeta, usern: data["usern"], color: bya});
		socket.emit("changerlab", {chata: data["message"]});
		}//end if statement to be sure that password is okay
		else{
			socket.emit("changerlab", {chata: data["curroom"]});
		}
	}); 

	socket.on('what_rooms', function(data){
		//client asks what rooms are available, returns with message listing rooms
		for(i=0; i<roomarray.length; i++){
			var rtoadd=roomarray[i];
			socket.emit("add_this_room", {message: rtoadd});
		}

	});

	socket.on('message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
 
		console.log("message: "+data["message"]); // log it to the Node.JS output

			io.sockets.to(data["curchat"]).emit("message_to_client", {message:data["message"], color:data["usern"], usern: data["usern"]}); //broadcast to not main
	});

	socket.on('privames', function(data){
		//takes in username you want to message, finds their socketid, sends them a private messa
		var adi="For "+ data["friend"]+ " eyes only: "+ data["message"];
		var colo='red';
		io.sockets.to(socks[socks.indexOf(data["friend"])-1]).emit("message_to_client", {message:adi, color: colo, usern: data["usern"]});
		socket.emit("message_to_client", {message:adi, usern: data["usern"], color: colo} );
		//var temptest=socks[socks.indexOf(data["friend"])-1];

	});

	socket.on('kicka', function(data){
		//check to see if person sending kickout request owns the room
		if(data["usern"]==ownera[roomarray.indexOf(data["curroom"])]){		
			io.sockets.to(socks[socks.indexOf(data["friend"])-1]).emit("goodbye", {usern: data["friend"]});
		}
		else{
			adi="You can't kick that person out. You are not the owner of this chat room.";
			socket.emit("message_to_client", {message:adi, color:adi, usern: data["usern"]});
			
		}
	});

	socket.on('banna', function(data){
		//ban person permanently, check to see if ban request comes from owner of room
		if(data["usern"]==ownera[roomarray.indexOf(data["curroom"])]){		
			io.sockets.to(socks[socks.indexOf(data["friend"])-1]).emit("goodbye", {usern: data["friend"]});
			banlist[roomarray.indexOf(data["curroom"])].push(data["friend"]);
		}
		else{
			adi="You can't ban that person out. You are not the owner of this chat room. Everyone else will be informed of your wish to have them banned.";
			socket.emit("message_to_client", {message:adi, color: adi, usern: data["usern"]});
			var aditwo=data["usern"]+" wishes for " + data["friend"] + " to be banned permanently. The owner has been informed.";
			//tell owner of chatroom
			var adithree="Hi owner of "+ data["curroom"]+ " there has been a request to have " + data["friend"] + " be banned permanently.";
			var fakeper='Chat Room Website';
			io.sockets.to(socks[socks.indexOf(ownera[roomarray.indexOf(data["curroom"])])-1]).emit("message_to_client", {color: fakeper, message: adithree, usern: fakeper});
			io.sockets.in(data["curroom"]).emit("message_to_client", {message: aditwo, usern: data["usern"], color: aditwo});
			
		}
	});

});
